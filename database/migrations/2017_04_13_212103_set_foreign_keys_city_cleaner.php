<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetForeignKeysCityCleaner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_cleaner', function (Blueprint $table) {
            $table->foreign('cleaner_id')->references('id')->on('cleaners');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_cleaner', function (Blueprint $table) {
            $table->dropForeign(['cleaner_id']);
            $table->dropForeign(['city_id']);
        });
    }
}
