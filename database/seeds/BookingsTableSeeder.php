<?php

use Illuminate\Database\Seeder;

class BookingsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('bookings')->insert([
            ['customer_id' => '1', 'cleaner_id' => '2', 'city_id' => '1', 'date_from' => '2017-04-18 17:00:00', 'date_to' => '2017-04-18 18:00:00'],
            ['customer_id' => '2', 'cleaner_id' => '3', 'city_id' => '2', 'date_from' => '2017-04-18 18:00:00', 'date_to' => '2017-04-18 20:00:00'],
            ['customer_id' => '3', 'cleaner_id' => '1', 'city_id' => '1', 'date_from' => '2017-04-18 19:00:00', 'date_to' => '2017-04-18 21:00:00'],
            ['customer_id' => '1', 'cleaner_id' => '2', 'city_id' => '2', 'date_from' => '2017-04-18 20:00:00', 'date_to' => '2017-04-18 22:00:00'],
            ['customer_id' => '2', 'cleaner_id' => '3', 'city_id' => '3', 'date_from' => '2017-04-18 21:00:00', 'date_to' => '2017-04-18 22:00:00'],
            ['customer_id' => '3', 'cleaner_id' => '1', 'city_id' => '2', 'date_from' => '2017-04-18 10:00:00', 'date_to' => '2017-04-18 13:00:00'],
            ['customer_id' => '1', 'cleaner_id' => '2', 'city_id' => '5', 'date_from' => '2017-04-18 11:00:00', 'date_to' => '2017-04-18 13:00:00'],
            ['customer_id' => '2', 'cleaner_id' => '3', 'city_id' => '6', 'date_from' => '2017-04-19 12:00:00', 'date_to' => '2017-04-19 13:00:00'],
            ['customer_id' => '3', 'cleaner_id' => '1', 'city_id' => '4', 'date_from' => '2017-04-19 13:00:00', 'date_to' => '2017-04-19 15:00:00'],
            ['customer_id' => '1', 'cleaner_id' => '2', 'city_id' => '1', 'date_from' => '2017-04-19 14:00:00', 'date_to' => '2017-04-19 16:00:00'],
            ['customer_id' => '2', 'cleaner_id' => '3', 'city_id' => '2', 'date_from' => '2017-04-19 15:00:00', 'date_to' => '2017-04-19 17:00:00'],
            ['customer_id' => '3', 'cleaner_id' => '1', 'city_id' => '1', 'date_from' => '2017-04-19 18:00:00', 'date_to' => '2017-04-19 20:00:00'],
        ]);
    }

}
