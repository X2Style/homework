<?php

use Illuminate\Database\Seeder;

class CleanersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('cleaners')->insert([
            ['first_name' => 'First', 'last_name' => 'Cleaner', 'email' => 'x8786@yandex.ua', 'quality_score' => '4.2'],
            ['first_name' => 'Second', 'last_name' => 'Cleaner', 'email' => 'a@x2.cx.ua', 'quality_score' => '3.7'],
            ['first_name' => 'Third', 'last_name' => 'Cleaner', 'email' => 'no-reply@x2.cx.ua', 'quality_score' => '3.1'],
        ]);
        
        DB::table('city_cleaner')->insert([
            ['cleaner_id' => '1', 'city_id' => '1'],
            ['cleaner_id' => '1', 'city_id' => '2'],
            ['cleaner_id' => '1', 'city_id' => '4'],
            ['cleaner_id' => '2', 'city_id' => '1'],
            ['cleaner_id' => '2', 'city_id' => '2'],
            ['cleaner_id' => '2', 'city_id' => '5'],
            ['cleaner_id' => '3', 'city_id' => '2'],
            ['cleaner_id' => '3', 'city_id' => '3'],
            ['cleaner_id' => '3', 'city_id' => '6'],
        ]);
    }

}
