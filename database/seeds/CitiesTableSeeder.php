<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('cities')->insert([
            ['name' => 'Kyiv'],
            ['name' => 'Dnipro'],
            ['name' => 'Lviv'],
            ['name' => 'Odessa'],
            ['name' => 'Kharkiv'],
            ['name' => 'Poltava']
        ]);
    }

}
