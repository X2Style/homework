<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('customers')->insert([
            ['first_name' => 'First', 'last_name' => 'Customer', 'email' => 'x8786@yandex.ua', 'phone' => '(012) 345-67-89'],
            ['first_name' => 'Second', 'last_name' => 'Customer', 'email' => 'a@x2.cx.ua', 'phone' => null],
            ['first_name' => 'Third', 'last_name' => 'Customer', 'email' => 'no-reply@x2.cx.ua', 'phone' => '(011) 123-45-67'],
        ]);
    }

}
