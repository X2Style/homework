const {mix} = require('laravel-mix');
let webpack = require('webpack');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
        .sass('resources/assets/sass/app.scss', 'public/css')
        .webpackConfig({
//            output: {
//                filename: 'bundle.js',
//                path: __dirname + "/dist"
//            },
//            module: {
//                
//                rules: [
//                    {
//                        test: /\.js?$/,
//                        exclude: [
//                            path.resolve(__dirname, "node_modules/moment/")
//                        ],
//                        loader: "babel-loader"
//                    }
//                ]
//            },
            resolve: {
                //modulesDirectories: ['node_modules'],
                alias: {
                    moment: path.resolve(__dirname, "resources/assets/bower_components/moment/moment.js"),
                },
            },
            plugins:
                    [
                        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en-gb/)
                    ]

        }
        );
