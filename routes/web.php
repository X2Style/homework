<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'HomeController@index');

//Booking routes
Route::get('/booking', 'BookingController@index');
Route::post('/booking', 'BookingController@store');
Route::get('/booking/success', 'BookingController@success');
Route::get('/booking/failure', 'BookingController@failure');

//Admin routes
Route::group(['prefix' => 'admin'], function () {

    //Auth::routes();
    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    //Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    //Route::post('register', 'Auth\RegisterController@register');
    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    //Admin secure routes
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'AdminController@index');

        Route::resource('customer', 'Admin\CustomerController');
        Route::resource('city', 'Admin\CityController');
        Route::resource('booking', 'Admin\BookingController');
        Route::resource('cleaner', 'Admin\CleanerController');
    });
});

