@extends('layouts.app')

@section('content')
<div class="row booking-details">
    <div class="row">
        @include('partials.flash')
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 booking-info">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Booking details</div>
                        <div class="panel-body">
                            <form name="bookit-index" id="bookit-index" method="POST" action="/booking">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                            <label for="date">Date</label>
                                            <div class="input-group date" id="datepicker-date-index">
                                                <input type="datetime" class="form-control" name="date" id="date" placeholder="dd-mm-yyyy" value="{{ old('date') }}">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                            @if ($errors->has('date'))
                                            <span class="help-block has-error">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                                            <label for="duration">Duration</label>
                                            @include('partials.booking.duration_select', ['duration' => old('duration'), 'durationMax' => 8])
                                            @if ($errors->has('duration'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('duration') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                                            <label for="city">City</label>
                                            <select class="form-control" name="city_id" id="city">
                                                @foreach ($cities as $city)
                                                <option value="{{$city->id}}"{{ $city->id == old('city_id') ? ' selected' : '' }}>{{$city->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('city_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label for="first_name">First name</label>
                                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" value="{{ old('first_name') }}" required>
                                            @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <label for="last_name">Last name</label>
                                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" value="{{ old('last_name') }}" required>
                                            @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email">E-mail</label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="mail@address.com" value="{{ old('email') }}" required>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone">Phone <small>(optional)</small></label>
                                            <input type="tel" class="form-control" name="phone" id="phone" placeholder="(067) 123-45-67" value="{{ old('phone') }}">
                                            @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-lg btn-green">Try again</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
