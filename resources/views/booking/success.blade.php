@extends('layouts.app')

@section('content')
<div class="row booking-details">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center">Your booking accepted</h1>
            <h2 class="text-center">We'll be in touch you soon</h2>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 booking-info">
                    <div class="panel panel-success">
                        <div class="panel-heading">Booking details</div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-right"><strong>Date:</strong></td>
                                    <td>{{ session('date') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Time:</strong></td>
                                    <td>{{ session('time') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Duration:</strong></td>
                                    <td>{{ session('duration') }}</td>
                                </tr>
                            </table>
                            <p>Cleaner details</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-right"><strong>Name:</strong></td>
                                    <td>{{ session('name') }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Rating:</strong></td>
                                    <td>
                                        @for ($i = 1; $i <= session('quality_score'); $i++)
                                        <span class="glyphicon glyphicon-star"></span> 
                                        @endfor
                                        {{ session('quality_score') }} {{session('quality_score') > 1 ? 'Points' : 'Point'}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
