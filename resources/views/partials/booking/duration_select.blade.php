<select class="form-control" name="duration" id="duration">
    @for ($i = 1; $i <= $durationMax; $i++)
    <option value="{{ $i }}" {{ $i == $duration ? 'selected' : '' }}>{{ $i }} {{ $i > 1 ? 'Hours' : 'Hour'}}</option>
    @endfor
</select>