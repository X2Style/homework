@if (Auth::guest())

@else
<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    <li>
        <a href="/admin"><span class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}</a>
    </li>
    <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>
</ul>
@endif