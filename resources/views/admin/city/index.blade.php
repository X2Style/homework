@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">City list</div>
                <div class="panel-body">
                    @if ($cities->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach($cities as $city)
                        <tr>
                            <td>
                                <a href="{{ route('city.show', ['id' => $city->id]) }}">{{ $city->name }}</a>
                            </td>
                            <td class="text-right">
                                <form action="{{ route('city.destroy', ['id' => $city->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('city.edit', ['id' => $city->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                    <div class="alert alert-danger" role="alert">No cities defined</div>
                    @endif
                    <div class="text-center">
                        <a class="btn btn-default" href="{{ route('city.create') }}">Add city</a>
                    </div>
                    <div class="text-center">{{ $cities->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
