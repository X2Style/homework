@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">City</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-8">
                                    {{ $city->name }}
                                </div>
                                <div class="col-md-4 text-right">
                                    <form action="{{ route('city.destroy', ['id' => $city->id]) }}" method="POST">
                                        <div class="btn-group-xs" role="group" aria-label="...">
                                            <a href="{{ route('city.edit', ['id' => $city->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger">
                                                <span class="glyphicon glyphicon-trash"></span> Remove
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                    @if ($cleaners->isNotEmpty())
                    <p><strong>Cleaners</strong></p>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>Cleaner name</td>
                                <td>Quality score</td>
                                <td></td>
                            </tr>
                        </thead>
                        @foreach ($cleaners as $cleaner)
                        <tr>
                            <td><a href="{{ route('cleaner.show', ['id' => $cleaner->id]) }}">{{ $cleaner->first_name }} {{ $cleaner->last_name }}</a></td>
                            <td>{{ $cleaner->quality_score }}</td>
                            <td class="text-right">
                                <form action="{{ route('cleaner.destroy', ['id' => $cleaner->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('cleaner.edit', ['id' => $cleaner->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <div class="text-center">{{ $cleaners->links() }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
