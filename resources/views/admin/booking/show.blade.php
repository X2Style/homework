@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Booking</div>
                <div class="panel-body">
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Date from:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $booking->date_from }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Date to:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $booking->date_to }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Customer name:</strong>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('customer.show', ['id' => $booking->customer->id]) }}">
                                {{ $booking->customer->first_name }} {{ $booking->customer->last_name }}
                            </a>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Customer phone:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $booking->customer->phone }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Customer e-mail:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $booking->customer->email }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Cleaner name:</strong>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('cleaner.show', ['id' => $booking->cleaner->id]) }}">
                                {{ $booking->cleaner->first_name }} {{ $booking->cleaner->last_name }}
                            </a>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>City:</strong>
                        </div>
                        <div class="col-md-6">
                            <a href="{{ route('city.show', ['id' => $booking->city->id]) }}">
                                {{ $booking->city->name }}
                            </a>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <form action="{{ route('booking.destroy', ['id' => $booking->id]) }}" method="POST">
                            <div class="btn-group" role="group" aria-label="...">
                                <a href="{{ route('booking.edit', ['id' => $booking->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash"></span> Remove
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
