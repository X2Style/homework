@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Booking edit</div>
                <div class="panel-body">
                    <form name="bookit-index" id="bookit-index" method="POST" action="{{ route('booking.update', ['id' => $booking->id]) }}">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('date_from') ? ' has-error' : '' }}">
                                    <label for="date">Date and time</label>
                                    <div class="input-group date" id="datepicker-date-edit">
                                        <input type="datetime" class="form-control" name="date" id="date" placeholder="dd-mm-yyyy" value="{{ date('d-m-Y H:i', strtotime($booking->date_from)) }}">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    @if ($errors->has('date'))
                                    <span class="help-block has-error">
                                        <strong>{{ $errors->first('date_from') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                                    <?php
                                    $date_from = date_create($booking->date_from);
                                    $date_to = date_create($booking->date_to);
                                    $diff = date_diff($date_from, $date_to);
                                    $duration = $diff->h;
                                    ?>
                                    <label for="duration">Duration</label>
                                    @include('partials.booking.duration_select', ['duration' => $duration, 'durationMax' => 8])
                                    @if ($errors->has('duration'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('duration') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
                                    <label for="city">City</label>
                                    <select class="form-control" name="city_id" id="city_id">
                                        @foreach ($cities as $city)
                                        <option value="{{$city->id}}"{{$booking->city_id == $city->id ? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('cleaner_id') ? ' has-error' : '' }}">
                                    <label for="city">Cleaner</label>
                                    <select class="form-control" name="cleaner_id" id="cleaner_id">
                                        @foreach ($cleaners as $cleaner)
                                        <option value="{{$cleaner->id}}"{{$booking->cleaner_id == $cleaner->id ? 'selected' : '' }}>{{$cleaner->first_name}} {{$cleaner->last_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('cleaner_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cleaner_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('customer_id') ? ' has-error' : '' }}">
                                    <label for="city">Customer</label>
                                    <select class="form-control" name="customer_id" id="customer_id">
                                        @foreach ($customers as $customer)
                                        <option value="{{$customer->id}}"{{$booking->customer_id == $customer->id ? 'selected' : '' }}>{{$customer->first_name}} {{$customer->last_name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('customer_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection