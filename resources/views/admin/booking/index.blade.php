@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Bookings</div>
                <div class="panel-body">
                    @if ($bookings->isNotEmpty())
                    <table class="table table-bordered">
                        <tr>
                            <td>ID</td>
                            <td>Customer Name</td>
                            <td>Customer phone</td>
                            <td>Customer e-mail</td>
                            <td>Cleaner name</td>
                            <td>City</td>
                            <td>Date from</td>
                            <td>Date to</td>
                            <td></td>
                        </tr>
                        @foreach($bookings as $booking)
                        <tr>
                            <td><a href="{{ route('booking.show', ['id' => $booking->id]) }}">{{ $booking->id }}</a></td>
                            <td><a href="{{ route('customer.show', ['id' => $booking->customer->id]) }}">{{ $booking->customer->first_name }} {{ $booking->customer->last_name }}</a></td>
                            <td>{{ $booking->customer->phone }}</td>
                            <td>{{ $booking->customer->email }}</td>
                            <td><a href="{{ route('cleaner.show', ['id' => $booking->cleaner->id]) }}">{{ $booking->cleaner->first_name }} {{ $booking->cleaner->last_name }}</a></td>
                            <td><a href="{{ route('city.show', ['id' => $booking->city->id]) }}">{{ $booking->city->name }}</a></td>
                            <td>{{ $booking->date_from }}</td>
                            <td>{{ $booking->date_to }}</td>
                            <td class="text-right">
                                <form action="{{ route('booking.destroy', ['id' => $booking->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('booking.edit', ['id' => $booking->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                    <div class="alert alert-danger" role="alert">No booking defined</div>
                    @endif
                    <div class="text-center">
                        <a class="btn btn-default" href="{{ route('booking.create') }}">Add booking</a>
                    </div>
                </div>
                <div class="text-center">{{ $bookings->links() }}</div>
            </div>
        </div>
    </div>
</div>
@endsection
