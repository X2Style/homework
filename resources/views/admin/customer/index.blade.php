@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customers</div>
                <div class="panel-body">
                    @if ($customers->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>    
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Phone number</th>
                            <th></th>
                        </tr>
                        </thead>
                        @foreach($customers as $customer)
                        <tr>
                            <td><a href="{{ route('customer.show', ['id' => $customer->id]) }}">{{ $customer->first_name }} {{ $customer->last_name }}</a></td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->phone }}</td>
                            <td class="text-right">
                                <form action="{{ route('customer.destroy', ['id' => $customer->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('customer.edit', ['id' => $customer->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                    <div class="alert alert-danger" role="alert">No customers defined</div>
                    @endif
                    <div class="text-center">
                        <a class="btn btn-default" href="{{ route('customer.create') }}">Add customer</a>
                    </div>
                    <div class="text-center">{{ $customers->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
