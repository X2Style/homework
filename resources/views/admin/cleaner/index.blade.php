@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cleaners</div>
                <div class="panel-body">
                    @if ($cleaners->isNotEmpty())
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Quality score</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach($cleaners as $cleaner)
                        <tr>
                            <td><a href="{{ route('cleaner.show', ['id' => $cleaner->id]) }}">{{ $cleaner->first_name }} {{ $cleaner->last_name }}</a></td>
                            <td>{{ $cleaner->email }}</td>
                            <td>{{ $cleaner->quality_score }}</td>
                            <td class="text-right">
                                <form action="{{ route('cleaner.destroy', ['id' => $cleaner->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('cleaner.edit', ['id' => $cleaner->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    @else
                    <div class="alert alert-danger" role="alert">No cleaners defined</div>
                    @endif
                    <div class="text-center">
                        <a class="btn btn-default" href="{{ route('cleaner.create') }}">Add cleaner</a>
                    </div>
                    <div class="text-center">{{ $cleaners->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
