@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cleaner add</div>
                <div class="panel-body">
                    <form action="{{ route('cleaner.store') }}" method="POST" id="a_cleaner">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">First name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" value="{{old('first_name')}}" required>
                            @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" value="{{old('last_name')}}" required>
                            @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="{{old('email')}}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('quality_score') ? ' has-error' : '' }}">
                            <label for="quality_score">Quality score</label>
                            <input type="number" step="0.1" class="form-control" name="quality_score" id="quality_score" placeholder="5.0" value="{{old('quality_score')}}" required>
                            @if ($errors->has('quality_score'))
                            <span class="help-block">
                                <strong>{{ $errors->first('quality_score') }}</strong>
                            </span>
                            @endif
                        </div>
                        <hr>
                        <strong>Cities</strong>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <?php
                                $count = 1;
                                $maxCount = count($cities);
                                ?>
                                @foreach($cities as $city)
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="city_id[]" value="{{ $city->id }}"> {{ $city->name }}
                                    </label>
                                </div>
                                @if($count % 3 == 0)
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                @endif
                                <?php $count++ ?>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-default">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
