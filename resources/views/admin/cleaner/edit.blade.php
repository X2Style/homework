@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cleaner edit</div>
                <div class="panel-body">
                    <form action="{{ route('cleaner.update', ['id' => $cleaner->id]) }}" method="POST" id="a_cleaner">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">First name</label>
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" value="{{$cleaner->first_name}}" required>
                            @if ($errors->has('first_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" value="{{$cleaner->last_name}}" required>
                            @if ($errors->has('last_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="{{$cleaner->email}}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('quality_score') ? ' has-error' : '' }}">
                            <label for="quality_score">Quality score</label>
                            <input type="text" class="form-control" name="quality_score" id="quality_score" placeholder="5.0" value="{{$cleaner->quality_score}}" required>
                            @if ($errors->has('quality_score'))
                            <span class="help-block">
                                <strong>{{ $errors->first('quality_score') }}</strong>
                            </span>
                            @endif
                        </div>
                        <hr>
                        <strong>Cities</strong>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <?php
                                $count = 1;
                                $maxCount = count($citiesList);
                                ?>
                                @foreach($citiesList as $city)
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="city_id[]" value="{{ $city->id }}"{{ (in_array($city->id, $citiesChecked)) ? ' checked' : '' }}> {{ $city->name }}
                                    </label>
                                </div>
                                @if($count % 3 == 0)
                            </div>
                            <div class="row">
                                <div class="col-md-2"></div>
                                @endif
                                <?php $count++ ?>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
