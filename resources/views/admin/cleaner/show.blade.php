@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Cleaner</div>
                <div class="panel-body">
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>First name:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $cleaner->first_name }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Last name:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $cleaner->last_name }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>E-mail:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $cleaner->email }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Quality score:</strong>
                        </div>
                        <div class="col-md-6">
                            {{ $cleaner->quality_score }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 text-right">
                            <strong>Cities:</strong>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                @foreach ($cleaner->cities as $city)
                                <li>{{$city->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <form action="{{ route('cleaner.destroy', ['id' => $cleaner->id]) }}" method="POST">
                            <div class="btn-group" role="group" aria-label="...">
                                <a href="{{ route('cleaner.edit', ['id' => $cleaner->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-trash"></span> Remove
                                </button>
                            </div>
                        </form>
                    </div>
                    @if ($bookings->isNotEmpty())
                    <p><strong>Bookings</strong></p>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Customer name</td>
                                <td>Customer phone</td>
                                <td>Customer e-mail</td>
                                <td>City</td>
                                <td>Booking start</td>
                                <td>Booking end</td>
                                <td></td>
                            </tr>
                        </thead>
                        @foreach ($bookings as $booking)
                        <tr>
                            <td><a href="{{ route('booking.show', ['booking' => $booking->id]) }}">{{ $booking->id }}</a></td>
                            <td><a href="{{ route('customer.show', ['id' => $booking->customer->id]) }}">{{ $booking->customer->first_name }} {{ $booking->customer->last_name }}</a></td>
                            <td>{{ $booking->customer->phone }}</td>
                            <td>{{ $booking->customer->email }}</td>
                            <td><a href="{{ route('city.show', ['id' => $booking->city->id]) }}">{{ $booking->city->name }}</a></td>
                            <td>{{ $booking->date_from }}</td>
                            <td>{{ $booking->date_to }}</td>
                            <td class="text-right">
                                <form action="{{ route('booking.destroy', ['id' => $booking->id]) }}" method="POST">
                                    <div class="btn-group-xs" role="group" aria-label="...">
                                        <a href="{{ route('booking.edit', ['id' => $booking->id]) }}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-trash"></span> Remove
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <div class="text-center">{{ $bookings->links() }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
