<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Booking information</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,300,600,500' rel='stylesheet' type='text/css'>
            <style type="text/css">

                html{
                    width: 100%; 
                }

                body{
                    width: 100%;  
                    margin:0; 
                    padding:0; 
                    -webkit-font-smoothing: antialiased;
                    mso-margin-top-alt:0px; 
                    mso-margin-bottom-alt:0px; 
                    mso-padding-alt: 0px 0px 0px 0px;
                    background: #ffffff;
                    font-family: 'Raleway', arial;
                    font-weight: 400;
                    letter-spacing:0.5px;
                }

                p,h1,h2,h3,h4{
                    margin-top:0;
                    margin-bottom:0;
                    padding-top:0;
                    padding-bottom:0;
                }

                table{
                    font-size: 14px;
                    border: 0;
                }

                img{
                    border: none!important;
                }

            </style>
    </head>
    <body style="margin: 0; padding: 0;">


        <!--  header  -->
        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
            <tr>
                <td>
                    <table width="600" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                        <tr>
                            <td width="100%" height="40"></td>
                        </tr>
                        <tr>
                            <td>
                                <!--  Logo  -->
                                <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="#" style="color: #6c6b6b; font-size: 24px; text-decoration: none;">Homework</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <!--  navigation menu  -->
                                <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tbody>
                                        <tr>
                                            <td height="3"></td>
                                        </tr>

                                        <tr>
                                            <td style="color: #6c6b6b; font-size: 14px; letter-spacing:0.5px;">
                                                <a href="#" style="color: #6c6b6b; text-decoration:none;">Some</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="#" style="color: #6c6b6b; text-decoration:none;">Menu</a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="#" style="color: #6c6b6b; text-decoration:none;">Here</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" height="40"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table><!--  end header  -->

        <!--  services  -->
        <table width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
            <tr>
                <td width="100%" height="24"></td>
            </tr>
            <tr>
                <td style="background-color: #38404b; color: #bcc9dd">
                    <h1 style="text-align: center;">Your booking accepted</h1>
                </td>
            </tr>
            <tr>
                <td style="background-color: #38404b; color: #bcc9dd">
                    <h2 style="text-align: center;">We'll be in touch you soon</h2>
                </td>
            </tr>
            <tr>
                <td width="100%" height="24"></td>
            </tr>
            <tr>
                <td>
                    <p style="padding:16px; color: #6c6b6b; font-size: 14px; letter-spacing:0.5px;">Booking details</p>
                    <table width="600" border="1" cellpadding="16" cellspacing="0" style="border-collapse:collapse;  color: #6c6b6b; font-size: 14px; letter-spacing:0.5px;">
                        <tr>
                            <td style="text-align: right; border: 1px solid #D5D5D5; "><strong>Date:</strong></td>
                            <td style="border: 1px solid #D5D5D5; ">{{ $bookingInfo['date'] }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; border: 1px solid #D5D5D5; "><strong>Time:</strong></td>
                            <td style="border: 1px solid #D5D5D5; ">{{ $bookingInfo['time'] }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; border: 1px solid #D5D5D5; "><strong>Duration:</strong></td>
                            <td style="border: 1px solid #D5D5D5; ">{{ $bookingInfo['duration'] }}</td>
                        </tr>
                    </table>
                    <p style="padding:16px; color: #6c6b6b; font-size: 14px; letter-spacing:0.5px;">Cleaner details</p>
                    <table width="600" border="1" cellpadding="16" cellspacing="0" style="border-collapse:collapse;  color: #6c6b6b; font-size: 14px; letter-spacing:0.5px;">
                        <tr>
                            <td style="text-align: right; border: 1px solid #D5D5D5; "><strong>Name:</strong></td>
                            <td style="border: 1px solid #D5D5D5; ">{{ $bookingInfo['name'] }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right; border: 1px solid #D5D5D5; "><strong>Rating:</strong></td>
                            <td style="border: 1px solid #D5D5D5; ">
                                {{ $bookingInfo['quality_score'] }} {{$bookingInfo['quality_score'] > 1 ? 'Points' : 'Point'}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%" height="24"></td>
            </tr>
        </table><!--  end services  -->


        <!--  footer  -->
        <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#38404b" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
            <tr>
                <td width="100%" height="40"></td>
            </tr>
            <tr>
                <td style="text-align:center;">
                    <a href="#" style="color: #bcc9dd; font-size: 24px; text-decoration: none;">
                        Homework
                    </a>
                </td>
            </tr>
            <tr>
                <td width="100%" height="40"></td>
            </tr>
            <tr>
                <td style="text-align:center; color: #bcc9dd; font-size: 14px; letter-spacing:.5px;">
                    <a href="#" style="color: #bcc9dd; text-decoration:none;">Some</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" style="color: #bcc9dd; text-decoration:none;">Menu</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" style="color: #bcc9dd; text-decoration:none;">Here</a>
                </td>
            </tr>
            <tr>
                <td width="100%" height="40"></td>
            </tr>
        </table><!--  end footer  -->


    </body>
</html>