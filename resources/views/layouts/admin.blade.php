<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
        </script>

        <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <header>
            <div class="container-fluid h-background">
                <nav class="navbar navbar-default">
                    <div class="container-fluid"> 
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                            <a class="navbar-brand" href="{{ url('/') }}"><img src="/images/logo_full.png" alt="Homework"></a></div>
                        <div class="collapse navbar-collapse" id="defaultNavbar1">
                            @if (!Auth::guest())
                            <ul class="nav navbar-nav">
                                <li><a href="{{ route('booking.index') }}">Bookings</a></li>
                                <li><a href="{{ route('city.index') }}">Cities</a></li>
                                <li><a href="{{ route('cleaner.index') }}">Cleaners</a></li>
                                <li><a href="{{ route('customer.index') }}">Customers</a></li>
                            </ul>
                            @endif
                            @include('partials.navbar')
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        @include('partials.flash')
        @yield('content')
        <footer class="container">
            <hr>
            <div class="row">
                <div class="text-center small col-md-6 col-md-offset-3">
                    <p>Copyright &copy; 2017 &middot; All Rights Reserved &middot; <a href="/">Homework</a></p>
                </div>
            </div>
        </footer>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>