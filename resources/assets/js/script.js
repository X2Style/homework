moment().locale('en-gb', {
    week: {
        dow: 1, // Monday is the first day of the week.
        doy: 1  // The week that contains Jan 4th is the first week of the year.
    }
});

//date & time pickers
(function ($, undefined) {
    var timeNow = moment().add('1', 'h');
    $('#datepicker-date-index').datetimepicker({
        locale: 'en-gb',
        format: 'DD-MM-YYYY HH:mm',
        minDate: timeNow,
        stepping: 60,
        sideBySide: true
    });
    $('#datepicker-date-edit').datetimepicker({
        locale: 'en-gb',
        format: 'DD-MM-YYYY HH:mm',
        stepping: 60,
        sideBySide: true
    });
})(jQuery);

//Forms validation
(function ($, undefined) {
    //Phone validation rule
    $.validator.addMethod("phone", function (v, e) {
        return this.optional(e) || /^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$/.test(v);
    }, 'Must be a valid phone number: (067) 123-45-78');

    //Admin customer form's validate and masking
    $('#a_customer').validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2
            },
            last_name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                phone: true
            }
        }
    });
    $("#a_customer #phone").mask("(999) 999-99-99");

    //Admin customer form's validate
    $('#a_cleaner').validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2
            },
            last_name: {
                required: true,
                minlength: 2
            },
            quality_score: {
                required: true,
                range: [0, 5],
                step: 0.1
            }
        }
    });

    //Admin cities form's validate
    $('#a_city').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            }
        }
    });

    //Admin and home page customer form's validate and masking
    $('#bookit-index').validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2
            },
            last_name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            phone: {
                phone: true
            }
        }
    });
    $("#bookit-index #phone").mask("(999) 999-99-99");

})(jQuery);