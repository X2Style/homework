
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('../bower_components/jquery-mask-plugin/dist/jquery.mask');
require('../bower_components/jquery-validation/dist/jquery.validate');
require('../bower_components/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker');
require('./script');