<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InformCleaner extends Mailable {

    use Queueable,
        SerializesModels;

    public $bookingInfo;
    public $request;
    public $subject = 'Booking information';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookingInfo, $request) {
        $this->bookingInfo = $bookingInfo;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('mail.informcleaner');
    }

}
