<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cleaner extends Model {

    protected $fillable = ['first_name', 'last_name', 'quality_score', 'email'];

    public function cities() {
        return $this->belongsToMany(City::class);
    }

    public function bookings() {
        return $this->hasMany(Booking::class);
    }

}
