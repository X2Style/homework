<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Booking extends Model {

    protected $fillable = [
        'customer_id',
        'cleaner_id',
        'city_id',
        'date_from',
        'date_to'
    ];

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    public function cleaner() {
        return $this->belongsTo(Cleaner::class);
    }

    public function city() {
        return $this->belongsTo(City::class);
    }

    public function getBookedCleaners($fromDate, $toDate) {
        return DB::table('bookings')->
                        select('cleaner_id')->
                        //Cleaner can not be in 2+ cities in same time
                        //where('city_id', '=', $city)->
                        where(
                                function($q) use($fromDate, $toDate) {
                            $q->where([
                                ['date_from', '>=', $fromDate],
                                ['date_to', '<=', $toDate]
                            ])->orWhere([
                                ['date_from', '<', $fromDate],
                                ['date_to', '>', $fromDate]
                            ])->orWhere([
                                ['date_from', '<', $toDate],
                                ['date_to', '>', $toDate]
                            ])->orWhere([
                                ['date_from', '<', $fromDate],
                                ['date_to', '>', $toDate]
                            ]);
                        })->get();
    }

    public function getFreeCleaners($fromDate, $toDate, $city, $onlyOne = true) {
        $cleaners = $this->getBookedCleaners($fromDate, $toDate);
        $cleaners = $cleaners->map(function($x) {
                    return (array) $x;
                })->toArray();
        if (empty($cleaners)) {
            if ($onlyOne) {
                return City::find($city)->cleaners()->orderBy('cleaners.quality_score', 'DESC')->first();
            } else {
                return City::find($city)->cleaners()->orderBy('cleaners.quality_score', 'DESC');
            }
            //return City::find($city)->cleaners()->whereNotIn('city_cleaner.cleaner_id', $cleaners)->orderBy('quality_score', 'DESC')->first();
        } else {
            if ($onlyOne) {
                return City::find($city)->cleaners()->whereNotIn('city_cleaner.cleaner_id', $cleaners)->orderBy('quality_score', 'DESC')->first();
            } else {
                return City::find($city)->cleaners()->whereNotIn('city_cleaner.cleaner_id', $cleaners)->orderBy('quality_score', 'DESC');
            }
        }
    }

    public function getCleanerBookings($fromDate, $toDate, $cleanerId) {
        return DB::table('bookings')->
                select('cleaner_id')->
                //Cleaner can not be in 2+ cities in same time
                //where('city_id', '=', $city)->
                where('cleaner_id', $cleanerId)->
                where('id', '<>', $this->id)->
                where(
                        function($q) use($fromDate, $toDate) {
                    $q->where([
                        ['date_from', '>=', $fromDate],
                        ['date_to', '<=', $toDate]
                    ])->orWhere([
                        ['date_from', '<', $fromDate],
                        ['date_to', '>', $fromDate]
                    ])->orWhere([
                        ['date_from', '<', $toDate],
                        ['date_to', '>', $toDate]
                    ])->orWhere([
                        ['date_from', '<', $fromDate],
                        ['date_to', '>', $toDate]
                    ]);
                })->get();
    }

}
