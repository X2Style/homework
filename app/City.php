<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    //public $timestamps = false;
    protected $fillable = ['name'];

    public function cleaners() {
        return $this->belongsToMany(Cleaner::class);
    }

    public function customers() {
        return $this->hasMany(Customer::class);
    }

    public function bookings() {
        return $this->hasMany(Booking::class);
    }

}
