<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Customer;
use App\City;
use App\Mail\CustomerBookingInfo;
use App\Mail\InformCleaner;
use App\Http\Requests\StoreBookingRequest;

class BookingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cities = City::orderBy('name')->get();
        return view('booking.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookingRequest $request, Booking $booking) {
        $this->validate($request, [
            'date' => 'required|date|after:yesturday',
            //'time' => 'required|date_format:"H:i"',
            'duration' => 'required|integer|min:1|max:8',
            'city_id' => 'required|integer|exists:cities,id',
            'first_name' => 'required|min:2|max:100',
            'last_name' => 'required|min:2|max:100',
            'email' => 'required|email',
            'phone' => 'nullable|regex:#^\s*((\+?\s*(\(\s*)?3)?[\s-]*(\(\s*)?8[\s-]*)?(\(\s*)?0[\s-\(]*[1-9][\s-]*\d(\s*\))?([\s-]*\d){7}\s*$#'
        ]);
        
        $fromDate = date("Y-m-d H:i:s", strtotime(request('date')));
        $toDate = date("Y-m-d H:i:s", strtotime(request('date') . ' ' . ' + ' . request('duration') . 'hours'));
        $freeCleaner = $booking->getFreeCleaners($fromDate, $toDate, request('city_id'));

        if ($freeCleaner) {
            //Check customer
            //If exist - update
            //If not exist - create
            $customer = Customer::where('email', request('email'))->first();
            if ($customer) {
                Customer::find($customer->id)->update([
                    'first_name' => request('first_name'),
                    'last_name' => request('last_name'),
                    'phone' => request('phone')
                ]);
                $customerId = $customer->id;
            } else {
                $customerId = Customer::create([
                            'first_name' => request('first_name'),
                            'last_name' => request('last_name'),
                            'email' => strtolower(request('email')),
                            'phone' => request('phone')
                        ])->id;
            }

            Booking::create([
                'customer_id' => $customerId,
                'cleaner_id' => $freeCleaner->id,
                'city_id' => request('city_id'),
                'date_from' => $fromDate,
                'date_to' => $toDate
            ]);

            $bookingInfo = [
                'date' => date('d-m-Y', strtotime(request('date'))),
                'time' => date('H:i', strtotime(request('date'))),
                'duration' => request('duration') . ' ' . (request('duration') > 1 ? 'Hours' : 'Hour'),
                'city' => City::find(request('city_id'))->name,
                'name' => $freeCleaner->first_name . ' ' . $freeCleaner->last_name,
                'quality_score' => $freeCleaner->quality_score
            ];

            //Send mail
            \Mail::to(request('email'))->send(new CustomerBookingInfo($bookingInfo));
            \Mail::to($freeCleaner->email)->send(new InformCleaner($bookingInfo, $request));

            //return view('booking.success', compact('freeCleaner', 'request'));
            return redirect('/booking/success')->with($bookingInfo);
        } else {
            session()->flash('message-failure', 'Free cleaner not found. Try to choose another time.');
            //return view('booking.failure', compact('freeCleaner', 'request', 'cities'));
            return redirect('/booking/failure')->withInput()->with('error-key', true);
        }


        //return view('booking.create', compact('freeCleaner', 'request'));
    }

    public function success() {
        if (session()->has('date') && session()->has('quality_score')) {
            return view('booking.success');
        } else {
            return redirect('/');
        }
    }

    public function failure() {
        if (session()->has('error-key')) {
            $cities = City::all();
            return view('booking.failure', compact('cities'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show() {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
