<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\City;
use App\Customer;
use App\Cleaner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\CustomerBookingInfo;
use App\Mail\InformCleaner;
use App\Http\Requests\StoreBookingRequest;
use App\Http\Requests\UpdateBookingRequest;

class BookingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $bookings = Booking::with('customer', 'cleaner', 'city')->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.booking.index', compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $cities = City::all();
        return view('admin.booking.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookingRequest $request, Booking $booking) {
        $fromDate = date("Y-m-d H:i:s", strtotime(request('date')));
        $toDate = date("Y-m-d H:i:s", strtotime(request('date') . ' ' . ' + ' . request('duration') . 'hours'));
        $freeCleaner = $booking->getFreeCleaners($fromDate, $toDate, request('city_id'));

        if ($freeCleaner) {
            //Check customer
            //If exist - update
            //If not exist - create
            $customer = Customer::where('email', request('email'))->first();
            if ($customer) {
                Customer::find($customer->id)->update([
                    'first_name' => request('first_name'),
                    'last_name' => request('last_name'),
                    'phone' => request('phone')
                ]);
                $customerId = $customer->id;
            } else {
                $customerId = Customer::create([
                            'first_name' => request('first_name'),
                            'last_name' => request('last_name'),
                            'email' => strtolower(request('email')),
                            'phone' => request('phone')
                        ])->id;
            }

            Booking::create([
                'customer_id' => $customerId,
                'cleaner_id' => $freeCleaner->id,
                'city_id' => request('city_id'),
                'date_from' => $fromDate,
                'date_to' => $toDate
            ]);

            $bookingInfo = [
                'date' => date('d-m-Y', strtotime(request('date'))),
                'time' => date('H:i', strtotime(request('date'))),
                'duration' => request('duration') . ' ' . (request('duration') > 1 ? 'Hours' : 'Hour'),
                'city' => City::find(request('city_id'))->first()->name,
                'name' => $freeCleaner->first_name . ' ' . $freeCleaner->last_name,
                'quality_score' => $freeCleaner->quality_score
            ];

            //Send mail
            \Mail::to(request('email'))->send(new CustomerBookingInfo($bookingInfo));
            \Mail::to($freeCleaner->email)->send(new InformCleaner($bookingInfo, $request));

            session()->flash('message-success', 'Booking was created');
            return redirect()->route('booking.index');
        } else {
            session()->flash('message-failure', 'Free cleaner not found. Try to choose another time.');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking) {
        return view('admin.booking.show', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking) {
        $cities = City::all();
        $cleaners = Cleaner::all();
        $customers = Customer::all();
        return view('admin.booking.edit', compact('booking', 'cities', 'cleaners', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookingRequest $request, Booking $booking) {
        //Check - if cleaner or city changed
        if ($booking->city_id != $request->city_id || $booking->cleaner_id != $request->cleaner_id) {
            $cleaner = City::find($request->city_id)->cleaners()->where('cleaners.id', $request->cleaner_id)->first();

            if (!$cleaner) {
                session()->flash('message-failure', 'This cleaner does not work in this city.');
                return redirect()->back()->withInput();
            }
        }
        $fromDate = date("Y-m-d H:i:s", strtotime($request->date));
        $toDate = date("Y-m-d H:i:s", strtotime($request->date . ' ' . ' + ' . $request->duration . 'hours'));
        $cleaner = $booking->getCleanerBookings($fromDate, $toDate, $request->cleaner_id);
        if ($cleaner->isNotEmpty()) {
            session()->flash('message-failure', 'This cleaner booked at this time.');
            return redirect()->back()->withInput();
        } else {
            $booking->update([
                'customer_id' => $request->customer_id,
                'cleaner_id' => $request->cleaner_id,
                'city_id' => $request->city_id,
                'date_from' => $fromDate,
                'date_to' => $toDate
            ]);
            session()->flash('message-success', 'Booking was updated.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking) {
        $booking->delete();
        session()->flash('message-success', 'Booking was removed');
        return redirect()->route('booking.index');
    }

}
