<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;

class CustomerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $customers = Customer::paginate(env('PAGINATION_ONPAGE', 10));

        return view('admin.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCustomerRequest $request) {
        Customer::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => strtolower(request('email')),
            'phone' => request('phone'),
        ]);

        session()->flash('message-success', 'Customer was created');

        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer) {
        $bookings = Booking::with('customer', 'cleaner', 'city')->where('customer_id', $customer->id)->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.customer.show', compact('customer', 'bookings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer) {
        return view('admin.customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, Customer $customer) {
        $customer->update(request(['first_name', 'last_name', 'phone', 'email']));
        session()->flash('message-success', 'Customer was updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer) {
        try {
            $customer->delete();
            session()->flash('message-success', 'Customer was removed');
        } catch (\Exception $e) {
            redirect()->route('customer.index')->with('message-failure', 'You can not delete a customer that has bookings.');
        }
        return redirect()->route('customer.index');
    }

}
