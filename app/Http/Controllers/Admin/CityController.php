<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Cleaner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCityRequest;

class CityController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cities = City::paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.city.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCityRequest $request) {
        City::create(request(['name']));
        session()->flash('message-success', 'City was stored');
        return redirect()->route('city.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city) {
        $cleaners = Cleaner::with('cities')->whereHas('cities', function($q) use ($city) {
                    $q->where('id', $city->id);
                })->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.city.show', compact('city', 'cleaners'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city) {
        return view('admin.city.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCityRequest $request, City $city) {
        $city->name = request('name');
        $city->save();
        session()->flash('message-success', 'City was updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city) {
        try {
            $city->delete();
            session()->flash('message-success', 'City was removed');
        } catch (\Exception $e) {
            redirect()->route('city.index')->with('message-failure', 'You can not delete a city that has bookings or cleaners.');
        }
        return redirect()->route('city.index');
    }

}
