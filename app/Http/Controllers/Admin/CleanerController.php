<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Cleaner;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCleanerRequest;

class CleanerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cleaners = Cleaner::with('cities')->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.cleaner.index', compact('cleaners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cleaner $cleaner) {
        $cities = City::all();
        return view('admin.cleaner.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCleanerRequest $request) {
        $cleaner = Cleaner::create(request(['first_name', 'last_name', 'quality_score', 'email']));
        $cleaner->cities()->attach(request('city_id'));
        session()->flash('message-success', 'Cleaner was stored');
        return redirect()->route('cleaner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cleaner  $cleaner
     * @return \Illuminate\Http\Response
     */
    public function show(Cleaner $cleaner) {
        $bookings = Booking::with('customer', 'cleaner', 'city')->where('cleaner_id', $cleaner->id)->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.cleaner.show', compact('cleaner', 'bookings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cleaner  $cleaner
     * @return \Illuminate\Http\Response
     */
    public function edit(Cleaner $cleaner) {
        $citiesChecked = [];
        foreach ($cleaner->cities as $city) {
            $citiesChecked[] = $city->id;
        }
        $citiesList = City::all();
        return view('admin.cleaner.edit', compact('cleaner', 'citiesChecked', 'citiesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cleaner  $cleaner
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCleanerRequest $request, Cleaner $cleaner) {
        $cleaner->update(request(['first_name', 'last_name', 'quality_score', 'email']));
        $cleaner->cities()->sync(request('city_id'), true);
        session()->flash('message-success', 'Cleaner was updated');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cleaner  $cleaner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cleaner $cleaner) {
        try {
            $cleaner->delete();
            $cleaner->cities()->detach();
            session()->flash('message-success', 'Cleaner was removed');
        } catch (\Exception $e) {
            redirect()->route('cleaner.index')->with('message-failure', 'You can not delete a cleaner that has bookings.');
        }
        return redirect()->route('cleaner.index');
    }

}
