<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;

class AdminController extends Controller {
    public function index() {
        $bookings = Booking::with('customer', 'cleaner', 'city')->paginate(env('PAGINATION_ONPAGE', 10));
        return view('admin.home', compact('bookings'));
    }
}
