<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateBookingRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'date' => 'required|date|after:yesturday',
            'duration' => 'required|integer|min:1|max:8',
            'city_id' => 'required|integer|exists:cities,id',
            'cleaner_id' => 'required|integer|exists:cleaners,id',
            'customer_id' => 'required|integer|exists:customers,id'
        ];
    }

}
